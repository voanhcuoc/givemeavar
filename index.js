const express = require('express');
const fetch = require('node-fetch');

const selectJson = require('./selectJson.js');

const app = express();

const sendFileOpt = { root: __dirname };

app.get('/', (req, res) => {
    res.sendFile('./index.html', sendFileOpt);
});

app.get('/test.html', (req, res) => {
    res.sendFile('./test.html', sendFileOpt);
});

app.get('/analyzeJson.js', (req, res) => {
    res.set('Content-Type', 'application/javascript');
    res.sendFile('./analyzeJson.js', sendFileOpt);
});

app.get('/favicon.ico', (req, res) => {
    res.set('Content-Type', 'image/x-icon');
    res.sendFile('./favicon.ico', sendFileOpt);
});
// TODO need to use express.static

app.get('/var.js', (req, res) => {
    const link = decodeURI(req.query.link);
    const varname = decodeURI(req.query.varname);
    const select = req.query.select ? JSON.parse(decodeURI(req.query.select)) : undefined;
    console.log(link);
    console.log(select);
    fetch(link)
        .then(fetchRes => fetchRes.text())
        .then(json_str => {
            if (!select) return json_str;
            const doc = JSON.parse(json_str);
            const result = selectJson(doc, select);
            return JSON.stringify(result);
        })
        .then(json_str => `;const ${varname} = ${json_str};`)
        .then(js => {
            res.set('Content-Type', 'application/javascript');
            res.send(js);
        });
});

module.exports = app;
