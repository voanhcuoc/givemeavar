function getSchema(JSONstring) {
    return JSON.parse(JSONstring, collectSchema);
};

function collectSchema(key, value) {
    if (Array.isArray(value)) {
        return [value.reduce((collector, next) => ({ ...next, ...collector }), {})];
    } else if (
        (typeof value === 'string') ||
        (typeof value === 'number') ||
        (typeof value === 'boolean') ||
        (value === null)
    ) {
        return true;
    } else { // JSON object
        return value;
    }
};

// module.exports = getSchema;
