https://givemeavar.herokuapp.com/

## Development

```
npm install
npm start
```

or use heroku

```
heroku local web
```

Server will listen at port 5000

## Deployment

To heroku

```
heroku create <appname>
git push heroku master
```

## What to do now

The DOM manipulation code is beginning to doom now. So before any more features,
I would rather rewrite it in Vue 3, just for the sake of learning Vue 3, haha.