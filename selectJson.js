module.exports = function selectJson(doc, select) {
    // console.log(doc);
    if (Array.isArray(doc) && Array.isArray(select)) {
        return doc.map(el => selectJson(el, select[0]));
    } else if (select === true) {
        return doc;
    } else { // JSON object
        return Object.fromEntries(Object.entries(doc)
            .filter(([ key, value ]) => select[key])
            .map(([ key, value ]) => [ key, selectJson(value, select[key]) ]));
    };
};
